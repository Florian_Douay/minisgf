
#ifndef __SGF_TEST_TOOL__
#define __SGF_TEST_TOOL__

#include <stdio.h>

#include "sgf-disk.h"
#include "sgf-fat.h"
#include "sgf-dir.h"
#include "sgf-io.h"

enum EFlagPrint {

    FLAG_WITHOUT_META_DATA = 1,
    FLAG_META_DATA_ON_START = 2,
    FLAG_META_DATA_ON_FINISH = 4,
    FLAG_NEWLINE_EVERY_26_CHARS = 8,
};


void tool_print_file(char* file_name, enum EFlagPrint flag);

void tool_print_ofile(OFILE* file_reader, enum EFlagPrint flag);

void tool_append_file_n_time(char* file_name, int n);

void tool_append_file(char* file_name, char c);

void tool_print_metadata(OFILE* file, char *context);

char* get_long_string();

void tool_puts_long_string(OFILE* file_writer);


#endif
