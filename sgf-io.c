
/*
**  sgf-io.c
**
**  fonctions de lecture/ecriture (de caracteres et de blocs)
**  dans un fichier ouvert.
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "sgf-disk.h"
#include "sgf-data.h"
#include "sgf-fat.h"
#include "sgf-dir.h"
#include "sgf-io.h"

/**********************************************************************
 *
 *  FONCTIONS DE LECTURE DANS UN FICHIER
 *
 *********************************************************************/

/**********************************************************************
 Lire dans le "buffer" le bloc logique "nubloc" dans le fichier
 ouvert "file".
 
 ATTENTION: Faites en sorte de ne pas recommencer le chainage a
            partir du bloc 0 si cela n'est pas utile. Pour eviter ce
            parcours vous pouvez ajouter un champ a la structure OFILE
            qui vous donne l'adresse physique du bloc courant.
 *********************************************************************/

void sgf_read_block(OFILE* file, int block_number_asked)
{
    assert(file != NULL);
    assert(block_number_asked >= 0);
    assert(file->length > 0);

    int block_physical_address;

    int nombre_de_sauts = block_number_asked;

    int last_block_loaded = get_last_block_number_from_ptr(file->ptr);

    if (last_block_loaded < 0) last_block_loaded = 0;

    if (last_block_loaded != block_number_asked) {

        if (last_block_loaded <= block_number_asked) {

            nombre_de_sauts -= last_block_loaded;
            block_physical_address = file->current_block_addr;
        }
        else {

            block_physical_address = file->first_block_physical_address;
        }

        for(; nombre_de_sauts ; nombre_de_sauts--) {

            block_physical_address = get_fat(block_physical_address);

            assert(block_physical_address > get_fat_size_in_blocks());
        }

        file->current_block_addr = block_physical_address;
    }

    read_block(file->current_block_addr, &file->buffer);
}


/**********************************************************************
 Lire un caractere dans un fichier ouvert. Cette fonction renvoie
 -1 si elle trouve la fin du fichier.
 *********************************************************************/

int sgf_getc(OFILE* file)
{
    assert (file->mode == READ_MODE);

    /* detecter la fin de fichier */
    if (file->ptr >= file->length) {
        return (-1);
    }

    /* si le buffer est vide, le remplir */
    if (get_pos_in_block_from_ptr(file->ptr) == 0) {

        sgf_read_block(file, get_current_block_pointed_by_ptr(file->ptr));
    }

    return (int) file->buffer[ (file->ptr++ % BLOCK_SIZE) ];
}

/**********************************************************************
 *
 *  FONCTIONS D'ECRITURE DANS UN FICHIER
 *
 *********************************************************************/

/**********************************************************************
 Ajouter le bloc contenu dans le tampon au fichier ouvert decrit
 par "f".
 *********************************************************************/

void sgf_append_block(OFILE* file)
{
    TBLOCK b;
    int block_physical_address;

    if (file->mode == WRITE_MODE || APPEND_MODE) {

        /* Rechercher un bloc libre sur disque */
        block_physical_address = alloc_block();
        assert (block_physical_address >= 0);

        /* Definir le nouveau bloc comme le suivant du bloc courant */

        if (file->current_block_addr != FAT_EOF) {

            set_fat(file->current_block_addr, block_physical_address);
        }

        /* Definir le nouveau bloc comme le dernier bloc du fichier */
        set_fat(block_physical_address, FAT_EOF);

        /* Definir le nouveau bloc comme le bloc courant et incrementer le nombre de blocs*/
        file->current_block_addr = block_physical_address;

        /* Mettre a jour l'adresse vers le dernier bloc (nouveau bloc) */
        file->last_block_physical_address = block_physical_address;

        /* Lire le contenu courant de l'inode */
        read_block(file->inode, &b.data);

        /* Met a jour la structure de l'inode */
        if (b.inode.first == FAT_EOF)
            b.inode.first = block_physical_address;

        b.inode.last = block_physical_address;
        b.inode.length = file->length;

        /* Sauvegarder le nouvel etat de l'inode */
        write_block(file->inode, &b.data);

    }
    else {

        panic("Utilisation de <<sgf_append_block>> incorrecte.");
    }
}


/**********************************************************************
 Ecrire le caractere "c" dans le fichier ouvert decrit par "file".
 *********************************************************************/

void sgf_putc(OFILE* file, char  c)
{
    assert (file->mode == WRITE_MODE || file->mode == APPEND_MODE);

    /* Remplace by the substitute character if the character is not standart ascii  */
    if (c < 0 || c > 127)   c = (char) SUBSTITUTE_CHARACTER;

    file->buffer[ get_pos_in_block_from_ptr(file->ptr) ] = c;
    file->length ++;

    if(get_pos_in_block_from_ptr(file->ptr) == BLOCK_SIZE-1) {

        write_block(file->current_block_addr, &file->buffer);
        update_inode_length(file);
    }
    else if(get_pos_in_block_from_ptr(file->ptr) == 0) {

        sgf_append_block(file);
    }

    file->ptr++;
}


/**********************************************************************
 ecrire la chaine de caractere "s" dans un fichier ouvert en ecriture
 decrit par "file".
 *********************************************************************/

void sgf_puts(OFILE* file, char* s)
{
    for (; (*s != '\0'); s++) {

        sgf_putc(file, *s);
    }
}


/**********************************************************************
 *
 *  FONCTIONS D'OUVERTURE, DE FERMETURE ET DE DESTRUCTION.
 *
 *********************************************************************/


void deallocate_inode(int adr_inode) {
    set_fat(adr_inode, FAT_FREE);
}

void deallocate_file_blocks(int first_block_physical_adr) {
    int previous_block_physical_adr;
    int block_physical_adr = first_block_physical_adr;
    while(block_physical_adr != FAT_EOF) {
        previous_block_physical_adr = block_physical_adr;
        block_physical_adr = get_fat(block_physical_adr);
        set_fat(previous_block_physical_adr, FAT_FREE);
    }
}


/************************************************************
 Detruire un fichier.
 ************************************************************/
/* made public for testing purposes
 * DOES NOT REMOVE FROM DIRECTORY */
void sgf_remove(int addr_inode)
{
    TBLOCK b;
    read_block(addr_inode, &b.data);
    int first_block_physical_adr = b.inode.first;

    deallocate_inode(addr_inode);
    deallocate_file_blocks(first_block_physical_adr);
    printf("INFO: removed file with inode %d, displaying disk free blocks\n", addr_inode);
    debug_dump_disk_free_space();
}


/************************************************************
 Ouvrir un fichier en ecriture seulement (NULL si echec).
 ************************************************************/

static  OFILE*  sgf_open_write(const char* nom)
{
    int inode, oldinode;
    OFILE* file;
    TBLOCK b;

    /* Rechercher un bloc libre sur disque */
    inode = alloc_block();
    assert (inode >= 0);

    /* Allouer une structure OFILE */
    file = malloc(sizeof(struct OFILE));
    if (file == NULL) return (NULL);
    
    /* preparer un inode vers un fichier vide */
    b.inode.length = 0;
    b.inode.first  = FAT_EOF;
    b.inode.last   = FAT_EOF;

    /* sauver ce inode */
    write_block(inode, &b.data);
    set_fat(inode, FAT_INODE);

    /* mettre a jour le repertoire */
    oldinode = add_inode(nom, inode);
    if (oldinode > 0) sgf_remove(oldinode);
    
    file->length  = 0;
    file->first_block_physical_address   = FAT_EOF;
    file->last_block_physical_address    = FAT_EOF;
    file->current_block_addr = FAT_EOF;
    file->inode   = inode;
    file->mode    = WRITE_MODE;
    file->ptr     = 0;

    return (file);
}


/************************************************************
 Ouvrir un fichier en lecture seulement (NULL si echec).
 ************************************************************/

static  OFILE*  sgf_open_read(const char* nom)
{
    int inode_number;
    OFILE* file;
    TBLOCK b;

    /* Chercher le fichier dans le repertoire */
    inode_number = find_inode(nom);
    if (inode_number < 0) return (NULL);
    
    /* Lire l'inode */
    read_block(inode_number, &b.data);
    
    /* Allouer une structure OFILE */
    file = malloc(sizeof(struct OFILE));
    if (file == NULL) return (NULL);
    
    file->length  = b.inode.length;
    file->first_block_physical_address   = b.inode.first;
    file->last_block_physical_address    = b.inode.last;
    file->inode   = inode_number;
    file->current_block_addr = b.inode.first;
    file->mode    = READ_MODE;
    file->ptr     = 0;
    
    return (file);
}

/************************************************************
 Ouvrir un fichier en ecriture a la suite (NULL si echec).
 ************************************************************/

static  OFILE*  sgf_open_append(const char* nom)
{
    OFILE* file = sgf_open_read(nom);

    if (file == NULL || file->length == 0) {

        if (file != NULL)
            sgf_close(file);
        return sgf_open_write(nom);
    }

    file->mode = APPEND_MODE;
    file->current_block_addr = file->last_block_physical_address;
    file->ptr = file->length;

    if (get_pos_in_block_from_ptr(file->length) != 0) {

        int block_to_load = get_current_block_pointed_by_ptr(file->ptr);
        sgf_read_block(file, block_to_load);
    }

    return (file);
}


/************************************************************
 Ouvrir un fichier (NULL si echec).
 ************************************************************/

OFILE* sgf_open (const char* nom, int mode)
{
    switch (mode)
    {
        case READ_MODE:  return sgf_open_read(nom);
        case WRITE_MODE:  return sgf_open_write(nom);
        case APPEND_MODE: return sgf_open_append(nom);
        default:          return (NULL);
    }
}


/************************************************************
 Fermer un fichier ouvert.
 ************************************************************/

void sgf_close(OFILE* file)
{
    if((file->mode == WRITE_MODE || file->mode == APPEND_MODE)) {

        if (get_pos_in_block_from_ptr(file->ptr) !=  0) {

            write_block(file->current_block_addr, &file->buffer);
            update_inode_length(file);
        }
    }
    else if (file->mode != READ_MODE) {

        panic("Impossible de fermer ce fichier avec sgf_close. Le mode courant n'a pas ete implemente.");
    }

    free(file);
}


/**********************************************************************
 initialiser le SGF
 *********************************************************************/

void init_sgf (void)
{
    init_sgf_disk();
    init_sgf_fat();
}

/**********************************************************************

 *********************************************************************/

int sgf_seek(OFILE* file, int pos)
{
    assert(file->mode == READ_MODE);

    if(pos < 0 || pos >= file->length) {

        printf("Incorrect position asked in function sgf_seek()\n");
        return -1;
    }

    int block_to_load = get_current_block_pointed_by_ptr(pos-1);

    if (block_to_load != get_last_block_number_from_ptr(file->ptr)) {

        sgf_read_block(file, block_to_load);
    }

    file->ptr = pos;

    return 0;
}

/**********************************************************************
 * Returns the number of the block pointed by ptr.
 *********************************************************************/

int get_current_block_pointed_by_ptr(int ptr) {

    return ptr / BLOCK_SIZE;
}

/**********************************************************************
 * Returns the number of the block supposed to be already loaded. Returns -1 if no block is loaded.
 *********************************************************************/

int get_last_block_number_from_ptr(int ptr) {

    int block =  get_current_block_pointed_by_ptr(ptr);

    if (get_pos_in_block_from_ptr(ptr) == 0)
        --block;

    return block;
}

/**********************************************************************

 *********************************************************************/

int get_pos_in_block_from_ptr(int ptr) {

    return ptr % BLOCK_SIZE;
}

/**********************************************************************

 *********************************************************************/

void update_inode_length(OFILE* file) {

    TBLOCK b;
    read_block(file->inode, &b.data);
    b.inode.length = file->length;
    write_block(file->inode, &b.data);
}

/**********************************************************************

 *********************************************************************/

int sgf_write(OFILE* file, char* data, int size) {

    if (file->mode != WRITE_MODE && file->mode != APPEND_MODE) return FORBIDDEN_MODE;
    if (size <= 0)                                             return INVALID_SIZE;
    if (data == NULL)                                          return INVALID_DATA;
    if (file->ptr != file->length)                             return PTR_NOT_AT_END_OF_FILE;

    int remaining = size;

    while (remaining > 0) {

        int nbre_utilises_dans_bloc_courant = get_pos_in_block_from_ptr(file->ptr);

        int taille_restant_disponible_dans_bloc_courant = BLOCK_SIZE - nbre_utilises_dans_bloc_courant;

        if (get_pos_in_block_from_ptr(file->ptr) == 0) {

            sgf_append_block(file);
        }

        if (remaining <= taille_restant_disponible_dans_bloc_courant) {

            memcpy (file->buffer + (get_pos_in_block_from_ptr(file->ptr)), (const void *) (data + (size-remaining)), remaining);

            file->length += remaining;
            file->ptr = file->length;

            remaining = 0;
        }
        else {

            memcpy (file->buffer + (get_pos_in_block_from_ptr(file->ptr)), (const void *) (data + (size-remaining)), taille_restant_disponible_dans_bloc_courant);

            file->length += taille_restant_disponible_dans_bloc_courant;
            file->ptr = file->length;

            remaining -= taille_restant_disponible_dans_bloc_courant;
        }

        write_block(file->current_block_addr, &file->buffer);
        update_inode_length(file);
    }

    return size;
}


char* str_from_estatus_sgf_write(enum ESTATUS_SGF_WRITE status) {

    switch (status) {
        case FORBIDDEN_MODE:
            return "FORBIDDEN_MODE";
        case INVALID_SIZE:
            return "INVALID_SIZE";
        case INVALID_DATA:
            return "INVALID_DATA";
        case PTR_NOT_AT_END_OF_FILE:
            return "PTR_NOT_AT_END_OF_FILE";
        default:
            return "UNKNOWN STATUS";
    }
}
