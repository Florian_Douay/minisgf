
/*
**  main.c
**
**  Utilisation des fonctions du mini SGF.
**
**  04/04/2007
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "sgf-disk.h"
#include "sgf-fat.h"
#include "sgf-dir.h"
#include "sgf-io.h"
#include "sgf-test-tools.h"

/**
 * @brief Appends to a new file in two times to test in second append doesn't overwrite something
 */
void test_append_twice() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    OFILE* file_writer = sgf_open("file_append.txt", APPEND_MODE);
    tool_print_metadata(file_writer, "after opening file_append.txt");

    sgf_puts(file_writer, "ab");
    sgf_close(file_writer);

    OFILE* file_writer2 = sgf_open("file_append.txt", APPEND_MODE);
    sgf_puts(file_writer2, "cd");
    sgf_close(file_writer2);

    tool_print_file("file_append.txt", FLAG_META_DATA_ON_START | FLAG_META_DATA_ON_FINISH);

    printf("\n\n###########################################\n");
}


void test_seek_every_8_chars(char* file_name) {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char* separator = "-";

    OFILE* file = sgf_open(file_name, READ_MODE);
    printf("file length : %d\n\n", file->length);
    for (int i = 0 ; i < file->length ; i = i+8) {

        sgf_seek(file, i);
        char c;
        if((c = sgf_getc(file)) > 0) {
            if (i != 0) printf(separator);
            printf("%c",c);
        }
        else {
            printf("ERROR GETC()\n");
        }
    }
    sgf_close(file);

    printf("\n\n###########################################\n");
}


void test_seek_backward_in_same_block(char* file_name) {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char c;
    int position_to_seek = 50;

    OFILE* file = sgf_open(file_name, READ_MODE);

    printf("file length : %d\n\n", file->length);

    sgf_seek(file, position_to_seek);

    if((c = sgf_getc(file)) > 0)
        printf("Character read after seek(%d): %c\n", position_to_seek, c);
    else
        printf("ERROR GETC()\n");

    //--------------------------------------------------

    position_to_seek = 20;
    sgf_seek(file,position_to_seek);

    if((c = sgf_getc(file)) > 0)
        printf("Character read after seek(%d): %c\n", position_to_seek, c);
    else
        printf("ERROR GETC()\n");

    sgf_close(file);

    printf("\n###########################################\n");
}


void test_seek_backward_change_block(char* file_name) {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char c;
    int position_to_seek = 150;

    OFILE* file = sgf_open(file_name, READ_MODE);

    printf("file length : %d\n\n", file->length);

    sgf_seek(file, position_to_seek);

    if((c = sgf_getc(file)) > 0)
        printf("Character read after seek(%d): %c\n", position_to_seek, c);
    else
        printf("ERROR GETC()\n");

    //--------------------------------------------------

    position_to_seek = 20;
    sgf_seek(file, 20);

    if((c = sgf_getc(file)) > 0)
        printf("Character read after seek(%d): %c\n", position_to_seek, c);
    else
        printf("ERROR GETC()\n");

    sgf_close(file);

    printf("\n###########################################\n");
}


void test_append_494() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    tool_append_file_n_time("test_append_494.txt", 494);
    tool_print_file("test_append_494.txt", FLAG_META_DATA_ON_START | FLAG_META_DATA_ON_FINISH | FLAG_NEWLINE_EVERY_26_CHARS);

    printf("\n###########################################\n");
}


void test_read_existing_file() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    tool_print_file("essai.txt", 0);

    printf("\n###########################################\n");
}


void test_puts() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char* file_name = "test_puts.txt";
    OFILE* file_writer = sgf_open(file_name, WRITE_MODE);
    tool_puts_long_string(file_writer);
    sgf_close(file_writer);
    tool_print_file(file_name, FLAG_META_DATA_ON_START | FLAG_META_DATA_ON_FINISH);

    printf("\n###########################################\n");
}


void test_open_write_on_existing_file() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char *file_name = "test_open_write.txt";

    printf(" {space free before write:\n   ");
    debug_dump_disk_free_space();
    printf("  }\n\n");
    OFILE* file_writer = sgf_open(file_name, WRITE_MODE);
    tool_puts_long_string(file_writer);
    sgf_close(file_writer);

    file_writer = sgf_open(file_name, WRITE_MODE);
    sgf_puts(file_writer, "efgh");
    sgf_close(file_writer);

    printf("\n");

    tool_print_file(file_name, 0);

    printf("\n\n###########################################\n");
}


void test_sgf_write_newfile() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char* empty_file_name = "test_sgf_write_empty.txt";
    OFILE* file_writer = sgf_open(empty_file_name, WRITE_MODE);

    int status = sgf_write(file_writer, get_long_string(), strlen(get_long_string()));
    if (status < 0) { printf("\n *** Error on sgf_writer(). Status : %s\n", str_from_estatus_sgf_write(status)); }

    sgf_close(file_writer);

    if (status >= 0) tool_print_file(empty_file_name, FLAG_WITHOUT_META_DATA);

    printf("\n\n###########################################\n");
}


void test_sgf_write_existing_file() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char* file_name = "test_sgf_write_exising.txt";
    OFILE* file_writer = sgf_open(file_name, WRITE_MODE);
    tool_puts_long_string(file_writer);
    sgf_close(file_writer);

    file_writer = sgf_open(file_name, APPEND_MODE);

    int status = sgf_write(file_writer, "123456789", 10); // should also write '\0'
    if (status < 0) { printf("\n *** Error on sgf_writer(). Status : %s\n", str_from_estatus_sgf_write(status)); }

    sgf_close(file_writer);

    if (status >= 0) tool_print_file(file_name, FLAG_WITHOUT_META_DATA);

    printf("\n\n###########################################\n");
}


void test_sgf_write_only_half_of_given_data() {

    printf(" ****** STARTING %s ******\n\n", __FUNCTION__);

    char* file_name = "test_sgf_write_existing.txt";
    OFILE* file_writer = sgf_open(file_name, WRITE_MODE);
    tool_puts_long_string(file_writer);
    sgf_close(file_writer);

    file_writer = sgf_open(file_name, APPEND_MODE);

    int status = sgf_write(file_writer, "123456789", 5);
    if (status < 0) { printf("\n *** Error on sgf_writer(). Status : %s\n", str_from_estatus_sgf_write(status));}

    sgf_close(file_writer);

    if (status >= 0) tool_print_file(file_name, FLAG_WITHOUT_META_DATA);

    printf("\n\n###########################################\n");
}


int main() {

    init_sgf();

    printf("\nlist directory :\n\n");
    list_directory();
    printf("\n###########################################\n");

    test_read_existing_file();
    test_seek_backward_in_same_block("essai.txt");
    test_seek_backward_change_block("essai.txt");
    test_seek_every_8_chars("essai.txt");
    test_append_twice();
    test_append_494();
    test_puts();
    test_open_write_on_existing_file();
    test_sgf_write_newfile();
    test_sgf_write_only_half_of_given_data();

    printf("\nlist directory :\n\n");
    list_directory();
    printf("\n###########################################\n");

    return (EXIT_SUCCESS);
}
