
#ifndef __SGF_IO__
#define __SGF_IO__

#define SUBSTITUTE_CHARACTER (26)

/**********************************************************************
 *
 *  La structure OFILE d�crit un fichier ouvert.
 *
 *  Cette structure comporte des informations sur l'implantation
 *  physique du fichier ainsi que des champs destin�s � d�crire
 *  l'�tat du fichier.
 *
 *********************************************************************/

#define READ_MODE       (0)
#define WRITE_MODE      (1)
#define APPEND_MODE     (2)

struct OFILE            /* "Un fichier ouvert"                  */
    {                   /* ------------------------------------ */
    int   length;       /* taille du fichier (en octets)        */
    int   first_block_physical_address;        /* adresse physique du premier bloc logique      */
    int   last_block_physical_address;         /* adresse physique du dernier bloc logique      */
    int   inode;        /* adresse de l'INODE (descripteur)     */
    int   ptr;          /* n� logique du prochain caract�re     */
    int   current_block_addr;        /* physiscal address       */
    int   mode;         /* READ_MODE ou WRITE_MODE              */
    BLOCK buffer;       /* buffer contenant le bloc courant     */
    };

typedef struct OFILE OFILE;

enum ESTATUS_SGF_WRITE {

    FORBIDDEN_MODE = -1,
    INVALID_SIZE   = -2,
    INVALID_DATA   = -3,
    PTR_NOT_AT_END_OF_FILE = -4
};

/**********************************************************************
 *
 *  ROUTINES DE GESTION DES E/S VERS DES FICHIERS OUVERTS (OFILE)
 *
 *********************************************************************/

/************************************************************
 *  Ecrire un caract�re/une cha�ne sur un fichier ouvert en
 *  �criture.
 ************************************************************/

    void sgf_puts (OFILE* f, char *s);
    void sgf_putc (OFILE* f, char  c);

/************************************************************
 *  Lire un caract�re sur un fichier ouvert en lecture.
 *  renvoyer -1 en cas de fin de fichier.
 ************************************************************/

    int sgf_getc (OFILE* f);

/************************************************************
 *  Ouvrir/Fermer/Partager un fichier.
 ************************************************************/

    OFILE* sgf_open  (const char *nom, int mode);
    void   sgf_close (OFILE* f);

/**********************************************************************
 * Initialiser le Syst�me de Gestion de Fichiers.
 *********************************************************************/

    void init_sgf ();

/**********************************************************************
 *
 *********************************************************************/

    int sgf_seek(OFILE* file, int pos);

/**********************************************************************
 *
 *********************************************************************/

    int get_current_block_pointed_by_ptr(int ptr);

/**********************************************************************
 *
 *********************************************************************/

    int get_last_block_number_from_ptr(int ptr);

/**********************************************************************
 *
 *********************************************************************/

    int get_pos_in_block_from_ptr(int ptr);

/**********************************************************************
 *
 *********************************************************************/

    void update_inode_length(OFILE* file);

/**********************************************************************
 *
 *********************************************************************/

    int sgf_write(OFILE* file, char* data, int size);

/**********************************************************************
 *
 *********************************************************************/

    char* str_from_estatus_sgf_write(enum ESTATUS_SGF_WRITE status);


#endif
