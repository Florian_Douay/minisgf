#include <assert.h>

#include "sgf-test-tools.h"


void tool_print_file(char* file_name, enum EFlagPrint flag) {

    OFILE* file_reader = sgf_open(file_name, READ_MODE);
    tool_print_ofile(file_reader, flag);
    sgf_close(file_reader);
}


void tool_print_ofile(OFILE* file_reader, enum EFlagPrint flags) {
    assert(file_reader->mode == READ_MODE);

    int i = 0; char c;

    if ((flags & FLAG_META_DATA_ON_START) != 0) tool_print_metadata(file_reader, "before starting printing file");

    while ((c = sgf_getc(file_reader)) > 0) {
        if ((flags & FLAG_NEWLINE_EVERY_26_CHARS) != 0) {
            if(i++%26==0) printf("\n");
        }

        putchar(c);
    }

    if ((flags & FLAG_META_DATA_ON_FINISH) != 0) { printf("\n\n");   tool_print_metadata(file_reader, "after printing file"); }
}


void tool_append_file_n_time(char* file_name, int n) {

    for (int i = 0 ; i < n; ++i) {
        tool_append_file(file_name, (char)'a' + i%26);
    }
}


void tool_append_file(char* file_name, char c) {

    OFILE* file_writer = sgf_open(file_name, APPEND_MODE);
    sgf_putc(file_writer, c);
    sgf_close(file_writer);
}


void tool_print_metadata(OFILE* file, char* context) {

    printf(" { current_block_addr: %d\n"
           "   first_block_physical_address: %d\n"
           "   last_block_physical_address: %d            <<<<<<  OFILE %s\n"
           "   ptr: %d, length: %d\n"
           "   current block pointed by ptr: %d\n"
           "   current block loaded from ptr: %d }\n\n"
           ,
           file->current_block_addr,
           file->first_block_physical_address,
           file->last_block_physical_address,
           context,
           file->ptr,
           file->length,
           get_current_block_pointed_by_ptr(file->ptr),
           get_last_block_number_from_ptr(file->ptr)
         );
}


char* get_long_string() {
    return "Voici un text un peut long qui doit faire plusieurs "
           "blocks, au moins 3 je pense, pour qu'il y ai un minimun de chainage de block "
           "pour bien tester le remove. Bon alors apparement c'est pas assez long "
           "donc il va falloir encore ecrire des choses donc il faut de l'inspiration "
           "et il est 2 heures 48 du matin donc ca deviens dur.";
}


void tool_puts_long_string(OFILE* file_writer) {
    assert(file_writer->mode == WRITE_MODE || file_writer->mode == APPEND_MODE);

    sgf_puts(file_writer, get_long_string());
}
